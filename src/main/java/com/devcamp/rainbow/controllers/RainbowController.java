package com.devcamp.rainbow.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class RainbowController {
    @GetMapping("/rainbow")
    public ArrayList<String> getRainbowList() {
        ArrayList<String> rainbowList = new ArrayList<>();

        rainbowList.add("red");
        rainbowList.add("orange");
        rainbowList.add("yellow");
        rainbowList.add("green");
        rainbowList.add("blue");
        rainbowList.add("indigo");
        rainbowList.add("violet");

        return rainbowList;
    }
}
